package com.frwk.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableRabbit
public class QueueConfig {

    @Value("${queue.gps.name}")
    private String maxtrackQueue;


    @Bean
    public Queue queue() {
        return new Queue(maxtrackQueue, true);
    }


}
