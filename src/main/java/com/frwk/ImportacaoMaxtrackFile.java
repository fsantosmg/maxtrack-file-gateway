package com.frwk;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.frwk.model.Position;
import com.frwk.parse.MaxTrackXMLPositionParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Component
public class ImportacaoMaxtrackFile {

    private static final Logger logger = LogManager.getLogger(ImportacaoMaxtrackFile.class);


    @Value("${tracking.files}")
    private String diretorioRastreio;

    @Value("${tracking.files.processed}")
    private String arquivosProcessados;

    @Value("${max.files.import}")
    private Integer maxFilesForImport;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private Queue queue;

    @Scheduled(fixedDelay = 1000)
    public void importacaoRastreamento() {


        File maxTrackFolder = new File(diretorioRastreio);

        if (!maxTrackFolder.exists()) {
            if (!maxTrackFolder.mkdirs()) {
                logger.info("############   Nao foi possivel criar o caminho " + diretorioRastreio);
                logger.info("#####   ERRO IMPORTACAO MAXTRACK ");
                return;
            }
            logger.info("Nao existe o caminho " + diretorioRastreio);
        }

        FilenameFilter filterName = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".xml");
            }
        };

        File[] files = maxTrackFolder.listFiles(filterName);

        if (files.length == 0) {
            logger.info("Não existem arquivos para serem processados.");
            return;

        } else {

            // Ordenda Pegando Data mais antiga primeiro
            Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
                }
            });
            Collection<File> trackingFiles = new ArrayList<File>();
            int index = 0;
            if (files.length >= maxFilesForImport) {
                for (File file : files) {
                    trackingFiles.add(file);
                    index++;
                    if (index >= maxFilesForImport)
                        break;
                }
            } else {
                trackingFiles = Arrays.asList(files);
            }
            int qtdArquivos = 0;

            if (trackingFiles != null) {
                qtdArquivos = trackingFiles.size();

                logger.info("## Total de arquivos a serem importados: " + qtdArquivos);

                int totalPosicoesImportadas = recebeArquivosImportados(trackingFiles);

                logger.info("## Total de positions importadas: " + totalPosicoesImportadas);


            } else {
                logger.info("Lista de arquivos a serem importados esta nula");
            }
        }
    }

    private Integer recebeArquivosImportados(Collection<File> trackingFiles) {

        Integer totalPositions = 0;

        for (File maxTrackXmlFile : trackingFiles) {

            MaxTrackXMLPositionParser parser = new MaxTrackXMLPositionParser();
            List<Position> positions = parser.getPositions(maxTrackXmlFile);
            totalPositions += positions.size();

            ObjectMapper mapper = new ObjectMapper();
            List<String> jsonPositions = new ArrayList<>();
            try {
                for (Position position : positions) {
                    jsonPositions.add(mapper.writeValueAsString(position));
                }

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            jsonPositions.forEach(position -> rabbitTemplate.convertAndSend(this.queue.getName(), position));




            try {
                Files.move(Paths.get(maxTrackXmlFile.toURI()), Paths.get(arquivosProcessados + maxTrackXmlFile.getName()));
            } catch (IOException e) {
                logger.error("Erro ao mover arquivo " + maxTrackXmlFile.getName());
                e.printStackTrace();
            }


        }

        return totalPositions;
    }


}
