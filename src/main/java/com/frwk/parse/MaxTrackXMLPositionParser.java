package com.frwk.parse;

import com.frwk.model.Position;
import com.frwk.model.PositionAdditional;
import com.frwk.model.TransmissionReasonEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;


public class MaxTrackXMLPositionParser {
    private final Logger logger = LogManager.getLogger(getClass().getName());

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");


    private List<Position> positions;


    public MaxTrackXMLPositionParser() {
        sdf.setTimeZone(TimeZone.getTimeZone("GMT0"));
        positions = new ArrayList<>();
    }

    public List<Position> getPositions(File xml) {
        // List<Position> sortedPositions = new ArrayList<MaxTrackXMLPositionParser>(positions);

        try {
            loadXMLData(xml);
        } catch (Exception e) {
            logger.error("Erro ao executar parse no xml.", e);
        }

        positions.sort(Comparator.comparing((p -> p.getMemoryIndex())));



        return positions;
    }


    private void loadXMLData(File xml) throws ParserConfigurationException, SAXException, IOException, DOMException {

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(xml);


        doc.getDocumentElement().normalize();

        NodeList positionsNode = doc.getElementsByTagName("POSITION");

        for (int i = 0; i < positionsNode.getLength(); i++) {
            Node positionNode = positionsNode.item(i);

            if (positionNode.getNodeType() == Node.ELEMENT_NODE && ((Element) positionNode.getParentNode()).getTagName().equals("POSITIONS")) {

                Element root = (Element) positionNode;

                Position p = new Position();
                if (root != null) {
                    if (root.getElementsByTagName("SERIAL").item(0) != null)
                        p.setSerial(root.getElementsByTagName("SERIAL").item(0).getTextContent());

                    // Assumindo que só exista 1 tag FIRMWARE em cada POSITION
                    if (root.getElementsByTagName("FIRMWARE").item(0) != null) {
                        Element firmwareNode = (Element) root.getElementsByTagName("FIRMWARE").item(0);
                        if (firmwareNode != null) {
                            if (firmwareNode.getElementsByTagName("MEMORY_INDEX").item(0) != null)
                                p.setMemoryIndex(new Long(firmwareNode.getElementsByTagName("MEMORY_INDEX").item(0).getTextContent()));

                            if (firmwareNode.getElementsByTagName("PROTOCOL").item(0) != null)
                                p.setProtocolo(new Integer(firmwareNode.getElementsByTagName("PROTOCOL").item(0).getTextContent()));

                            if (firmwareNode.getElementsByTagName("TRANSMISSION_REASON").item(0) != null) {
                                p.setTransmissionReasonEnum(TransmissionReasonEnum.getById(new Integer(firmwareNode.getElementsByTagName("TRANSMISSION_REASON").item(0).getTextContent())));
                            }

                        }
                    }

                    // Assumindo que só tem 1 tag GPS em cada POSITION
                    if (root.getElementsByTagName("GPS").item(0) != null) {
                        Element gpsNode = (Element) root.getElementsByTagName("GPS").item(0);
                        if (gpsNode != null) {
                            // Latitude e Longitude
                            if (gpsNode.getElementsByTagName("LATITUDE").item(0) != null)
                                p.setLatitude(new BigDecimal(gpsNode.getElementsByTagName("LATITUDE").item(0).getTextContent()));

                            if (gpsNode.getElementsByTagName("LONGITUDE").item(0) != null)
                                p.setLongitude(new BigDecimal(gpsNode.getElementsByTagName("LONGITUDE").item(0).getTextContent()));

                            // Altitude
                            if (gpsNode.getElementsByTagName("ALTITUDE").item(0) != null) {
                                Element altitudeNode = (Element) gpsNode.getElementsByTagName("ALTITUDE").item(0);
                                if (altitudeNode != null) {
                                    p.setAltitude(new Integer(altitudeNode.getTextContent()));
                                } else {
                                    p.setAltitude(0);
                                }
                            }

                            // flag gps
                            if (gpsNode.getElementsByTagName("FLAG_STATE").item(0) != null) {
                                Element flagGpsNode = (Element) gpsNode.getElementsByTagName("FLAG_STATE").item(0);

                                if (flagGpsNode.getElementsByTagName("GPS_ANTENNA_FAILURE").item(0) != null) {
                                    if (new Integer(flagGpsNode.getElementsByTagName("GPS_ANTENNA_FAILURE").item(0).getTextContent()) == 1)
                                        p.setFalhaGPS(true);
                                    else
                                        p.setFalhaGPS(false);
                                }
                                if (flagGpsNode.getElementsByTagName("GPRS_CONNECTION").item(0) != null) {
                                    if (new Integer(flagGpsNode.getElementsByTagName("GPRS_CONNECTION").item(0).getTextContent()) == 1)
                                        p.setSinalGprs(true);
                                    else
                                        p.setSinalGprs(false);
                                }
                            }

                            // Direcao
                            if (gpsNode.getElementsByTagName("COURSE").item(0) != null)
                                p.setDirecao(new Integer(gpsNode.getElementsByTagName("COURSE").item(0).getTextContent()));

                            // Velocidade
                            if (gpsNode.getElementsByTagName("SPEED").item(0) != null)
                                p.setVelocidade(new BigDecimal(gpsNode.getElementsByTagName("SPEED").item(0).getTextContent()));

                            // Hodometro
                            if (gpsNode.getElementsByTagName("HODOMETER").item(0) != null)
                                p.setHodometro(new Long(gpsNode.getElementsByTagName("HODOMETER").item(0).getTextContent()));

                            // Data Hora
                            if (gpsNode.getElementsByTagName("DATE").item(0) != null) {

                                DateTimeZone timeZone = DateTimeZone.forID("GMT0");
                                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZone(timeZone);
                                DateTime dateTime = formatter.parseDateTime(gpsNode.getElementsByTagName("DATE").item(0).getTextContent());

                                p.setDataHora(dateTime.toDate());

                            }
                        }
                    }

                    // Assumindo que só exista 1 tag HARDWARE_MONITOR em cada
                    // POSITION
                    if (root.getElementsByTagName("HARDWARE_MONITOR").item(0) != null) {
                        Element hardMonitorNode = (Element) root.getElementsByTagName("HARDWARE_MONITOR").item(0);

                        if (hardMonitorNode != null) {
                            // Horimetro
                            if (hardMonitorNode.getElementsByTagName("HOURMETER").item(0) != null) {
                                p.setHorimetro(new Long(hardMonitorNode.getElementsByTagName("HOURMETER").item(0).getTextContent()));
                            }

                            //Voltagem Bateria
                            if (hardMonitorNode.getElementsByTagName("POWER_SUPPLY").item(0) != null) {
                                p.setPowerSupply(new Double(hardMonitorNode.getElementsByTagName("POWER_SUPPLY").item(0).getTextContent()));
                            }
                            // RPM
                            if (hardMonitorNode.getElementsByTagName("RPM").item(0) != null) {
                                p.setRpm(new Integer(hardMonitorNode.getElementsByTagName("RPM").item(0).getTextContent()));
                            }
                        }

                    }

                    // INPUTS
                    if (root.getElementsByTagName("INPUTS").item(0) != null) {
                        Element inputNode = (Element) root.getElementsByTagName("INPUTS").item(0);
                        if (inputNode != null) {
                            // IGNITION
                            if (inputNode.getElementsByTagName("IGNITION").item(0) != null) {
                                if (new Integer(inputNode.getElementsByTagName("IGNITION").item(0).getTextContent()) == 1)
                                    p.setIgnicao(true);
                                else
                                    p.setIgnicao(false);
                            }

                            // PANICO
                            if (inputNode.getElementsByTagName("PANIC").item(0) != null) {
                                if (new Integer(inputNode.getElementsByTagName("PANIC").item(0).getTextContent()) == 1)
                                    p.setPanico(true);
                                else
                                    p.setPanico(false);
                            }

                            // INPUT1
                            if (inputNode.getElementsByTagName("INPUT1").item(0) != null) {
                                if (new Integer(inputNode.getElementsByTagName("INPUT1").item(0).getTextContent()) == 1)
                                    p.setInput1(true);
                                else
                                    p.setInput1(false);
                            }

                            // INPUT2
                            if (inputNode.getElementsByTagName("INPUT2").item(0) != null) {
                                if (new Integer(inputNode.getElementsByTagName("INPUT2").item(0).getTextContent()) == 1)
                                    p.setInput2(true);
                                else
                                    p.setInput2(false);
                            }

                            // INPUT1
                            if (inputNode.getElementsByTagName("INPUT3").item(0) != null) {
                                if (new Integer(inputNode.getElementsByTagName("INPUT3").item(0).getTextContent()) == 1)
                                    p.setInput3(true);
                                else
                                    p.setInput3(false);
                            }

                            // INPUT1
                            if (inputNode.getElementsByTagName("INPUT4").item(0) != null) {
                                if (new Integer(inputNode.getElementsByTagName("INPUT4").item(0).getTextContent()) == 1)
                                    p.setInput4(true);
                                else
                                    p.setInput4(false);
                            }
                        }
                    }


                    // OUTPUT
                    if (root.getElementsByTagName("OUTPUTS").item(0) != null) {
                        Element outputNode = (Element) root.getElementsByTagName("OUTPUTS").item(0);
                        if (outputNode != null) {

                            // INPUT1
                            if (outputNode.getElementsByTagName("OUTPUT1").item(0) != null) {
                                if (new Integer(outputNode.getElementsByTagName("OUTPUT1").item(0).getTextContent()) == 1)
                                    p.setOutput1(true);
                                else
                                    p.setOutput1(false);
                            }

                            // INPUT2
                            if (outputNode.getElementsByTagName("OUTPUT2").item(0) != null) {
                                if (new Integer(outputNode.getElementsByTagName("OUTPUT2").item(0).getTextContent()) == 1)
                                    p.setOutput2(true);
                                else
                                    p.setOutput2(false);
                            }

                            // INPUT1
                            if (outputNode.getElementsByTagName("OUTPUT3").item(0) != null) {
                                if (new Integer(outputNode.getElementsByTagName("OUTPUT3").item(0).getTextContent()) == 1)
                                    p.setOutput3(true);
                                else
                                    p.setOutput3(false);
                            }

                        }
                    }


                    // EMBEDDED
                    if (root.getElementsByTagName("EMBEDDED").item(0) != null) {
                        Element embeddedNode = (Element) root.getElementsByTagName("EMBEDDED").item(0);
                        if (embeddedNode != null) {
                            // DRIVER ID
                            if (embeddedNode.getElementsByTagName("DRIVER_ID").item(0) != null) {
                                p.setDriverID(new String(embeddedNode.getElementsByTagName("DRIVER_ID").item(0).getTextContent()));
                            }
                        }
                    }

                    // FLAG STATE H
                    if (root.getElementsByTagName("FLAG_STATE").item(0) != null) {
                        Element flagNode = (Element) root.getElementsByTagName("FLAG_STATE").item(1);
                        if (flagNode != null) {
                            // MODO ANTIFURTO
                            if (flagNode.getElementsByTagName("ANTI_THEFT_STATUS").item(0) != null) {
                                if (new Integer(flagNode.getElementsByTagName("ANTI_THEFT_STATUS").item(0).getTextContent()) == 3)
                                    p.setAntifurto(true);
                                else
                                    p.setAntifurto(false);
                            }
                            // FALHA NA BATERIA
                            if (flagNode.getElementsByTagName("BATTERY_FAILURE").item(0) != null) {
                                if (new Integer(flagNode.getElementsByTagName("BATTERY_FAILURE").item(0).getTextContent()) == 1)
                                    p.setFalhaBateria(true);
                                else
                                    p.setFalhaBateria(false);
                            }
                        }
                    }
                }

                //Extrai dados do MX100_DATA com memory_index igual ao desse position, sobrescrevendo os dados da tag position
                extractMX100DataIntoPosition(doc, p);
                extractAdditionalInfoPosition(doc, p);
                extractAdditionalTemperaturePosition(doc, p);
                extractTransparentDataPosition(doc, p);
                positions.add(p);
            }

        }
        //
    }

    private void extractMX100DataIntoPosition(Document doc, Position p) {
        NodeList mx100Nodes = doc.getElementsByTagName("MX100_DATA");

        for (int i = 0; i < mx100Nodes.getLength(); i++) {
            Node node = mx100Nodes.item(i);

            Element root = (Element) node;

            Long memoryIndex = new Long(root.getAttribute("memory_index"));
            String serial = root.getAttribute("mtc");

            //Verifica se esta MX100_DATA é do mesmo position que está sendo lido
            if (p.getMemoryIndex().equals(memoryIndex) && p.getSerial().equals(serial)) {

                Node positionNode = root.getElementsByTagName("POSITION").item(0);

                if (positionNode != null) {
                    Element positionElement = (Element) positionNode;

                    /*
                     * Atualiza os dados da Position com os lidos da tag MX100_DATA
                     */

                    // RPM
                    p.setRpm(new Integer(positionElement.getElementsByTagName("RPM").item(0).getTextContent()));

                    // SPEED
                    p.setVelocidade(new BigDecimal(positionElement.getElementsByTagName("SPEED").item(0).getTextContent()));

                    break;
                }
            }
        }
    }

    private void extractAdditionalInfoPosition(Document doc, Position p) {
        NodeList additionalNodes = doc.getElementsByTagName("ICC_ID_DATA");

        if (additionalNodes != null && additionalNodes.getLength() > 0) {
            for (int i = 0; i < additionalNodes.getLength(); i++) {
                Node node = additionalNodes.item(i);

                Element root = (Element) node;
                String serial = root.getAttribute("mxt");

//				System.out.println("TESTE ICCID NO MAXTRACKXML: "+new String(root.getElementsByTagName("ICC_ID").item(0).getTextContent()));
                if (p.getSerial().equals(serial)) {

                    p.setIccid(new String(root.getElementsByTagName("ICC_ID").item(0).getTextContent()));

                }
            }
        }
    }

    private void extractAdditionalTemperaturePosition(Document doc, Position p) {
        NodeList additionalNodes = doc.getElementsByTagName("WT110_DATA");

        if (additionalNodes != null && additionalNodes.getLength() > 0) {

            List<PositionAdditional> listAdditional = new ArrayList<PositionAdditional>();

            for (int i = 0; i < additionalNodes.getLength(); i++) {
                Node node = additionalNodes.item(i);

                Element root = (Element) node;
                String serial = root.getAttribute("MXT");
                Long memoryIndex = new Long(root.getAttribute("memory_index"));
                if (p.getSerial().equals(serial)) {
                    try {
                        p.setTemperature(new Integer(root.getElementsByTagName("INTERNAL_TEMPERATURE").item(0).getTextContent()));
                    } catch (Exception e) {
                        System.err.println("### Erro ao tentar converter Temperatura = " + root.getElementsByTagName("INTERNAL_TEMPERATURE").item(0).getTextContent());
                        p.setTemperature(0); //PALEATIVo PARA APRESENTACAO
                        //e.printStackTrace();
                    }
                }

                if (p.getSerial().equals(serial)) {
                    PositionAdditional additional = new PositionAdditional();
                    additional.setSerial(new String(root.getElementsByTagName("SERIAL").item(0).getTextContent()));
                    additional.setBatteryLevel(new Integer(root.getElementsByTagName("BATTERY_LEVEL").item(0).getTextContent()));
                    additional.setButtonStatus(new Integer(root.getElementsByTagName("BUTTON_STATUS").item(0).getTextContent()));
                    try {
                        additional.setInternalTemperature(new Integer(root.getElementsByTagName("INTERNAL_TEMPERATURE").item(0).getTextContent()));
                    } catch (Exception e) {
                        System.err.println("## Erro ao Realizar a Leitura da Temperatura atraves do WT110");
                        System.err.println("## Erro " + e.getMessage());
                    }
                    listAdditional.add(additional);
                }
            }
            p.setAdditional(listAdditional);
            //System.err.println("##### Lista de Informações de Temperatura: " + listAdditional.size() +"\n");
        }
    }

    private void extractTransparentDataPosition(Document doc, Position p) {

        XPath xPath = XPathFactory.newInstance().newXPath();

        String expression = "/POSITIONS/ADDITIONAL/TRANSPARENT_DATA[@mxt='" + p.getSerial() + "']/DATA/text()";

        try {

            String valor = (String) xPath.compile(expression).evaluate(doc, XPathConstants.STRING);
            p.setRs232(valor != null && !valor.trim().isEmpty() ? valor.trim() : null);

        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

    }
}
