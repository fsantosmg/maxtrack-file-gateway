package com.frwk.util;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GerenciadorDiretoriosMaxTrack {

    private static Logger logger = Logger.getLogger(GerenciadorDiretoriosMaxTrack.class.getName());


    // LINUX ARGOS
    private static String MAX_TRACK_XML_LOCATION = null;
    public static final String MAX_TRACK_XML_ERROR_LOCATION = MAX_TRACK_XML_LOCATION + "/error";
    public static final String MAX_TRACK_XML_IMPORTED_LOCATION = MAX_TRACK_XML_LOCATION + "/imported";
    public static final String MAX_TRACK_XML_ERROR_LOGS_LOCATION = MAX_TRACK_XML_LOCATION + "/logs/";

    private static String MAX_TRACK_XML_COMMAND_RESPONSE = null;


    public static void moveFileToError(File fileToMove) throws IOException {
        File maxTrackErrorFolder = new File(MAX_TRACK_XML_ERROR_LOCATION);
        if (!maxTrackErrorFolder.exists()) {
            if (!maxTrackErrorFolder.mkdirs()) {
                logger.log(Level.SEVERE, "Nao foi possivel localizar/criar o caminho " + MAX_TRACK_XML_ERROR_LOCATION);
                return;
            }
        }
        deleteFileIfExists(new File(maxTrackErrorFolder + File.separator + fileToMove.getName()));
       // FileCopyUtils.moveFileToDirectory(fileToMove, maxTrackErrorFolder, false);
    }

    public static void moveFileToImported(File fileToMove) throws IOException {
        File maxTrackImportedFolder = new File(MAX_TRACK_XML_IMPORTED_LOCATION);
        if (!maxTrackImportedFolder.exists()) {
            if (!maxTrackImportedFolder.mkdirs()) {
                logger.log(Level.SEVERE, "Nao foi possivel localizar/criar o caminho "
                        + MAX_TRACK_XML_IMPORTED_LOCATION);
                return;
            }
        }
        deleteFileIfExists(fileToMove);

    }

    public static void deleteFileIfExists(File f) throws IOException {
        logger.log(Level.FINE, "Tentando deletar arquivo " + f.getName());
        if (f.exists()) {
       //     FileUtils.forceDelete(f);
        }

        logger.log(Level.FINE, "Arquivo " + f.getName() + " deletado com sucesso!.");
    }

    public static String getMaxTrackCommandResponseFolder() {
        return MAX_TRACK_XML_COMMAND_RESPONSE;
    }

    public static void setMaxTrackFolder(String folder) {
        MAX_TRACK_XML_LOCATION = folder;
    }

    public static void setMaxTrackCommandResponseFolder(String folder) {
        MAX_TRACK_XML_COMMAND_RESPONSE = folder;
    }

    public static String getMaxTrackFolder() {
        return MAX_TRACK_XML_LOCATION;
    }
}
