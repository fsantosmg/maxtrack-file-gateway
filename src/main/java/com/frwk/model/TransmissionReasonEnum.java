package com.frwk.model;

public enum TransmissionReasonEnum {

    MODULO_ENERGIZADO(1, "Modulo Energizado"),
    RECONEXAO_GPRS(2, "Re-conex\u00E3o GPRS"),
    INTERVALO_TRANSMISSAO_PARADO(3, "Intervalo de Transmissao Parado"),
    INTERVALO_TRANSMISSAO_MOVIMENTO(4, "Intervalo de transmiss\u00E3o em movimento"),
    INTERVALO_TRANSMISSAO_PANICO(5, "Intervalo de transmiss\u00E3o em p\u00E2nico"),
    CONFIGURACAO_ENTRADA(6, "Configura\u00E7\u00E3o de entrada"),
    REQUISICAO_SERVIDOR(7, "Requisi\u00E7\u00E3o do servidor"),
    POSICAO_VALIDA_INTERVALO_TRANSMISSAO(8, "Posi\u00E7\u00E3o v\u00E1lida ap\u00F3s intervalo de transmiss\u00E3o"),
    IGNICAO_LIGADA(9, "Igni\u00E7\u00E3o ligada"),
    IGNICAO_DESLIGADA(10, "Igni\u00E7\u00E3o desligada"),
    PANICO_ATIVADO(11, "P\u00E2nico ativado"),
    PANICO_DESATIVADO(12, "P\u00E2nico Desativado"),
    ENTRADA1_ATIVADA(13, "Entrada 1 ativada"),
    ENTRADA1_ABERTA(14, "Entrada 1 desativada"),
    ENTRADA2_ATIVADA(15, "Entrada 2 ativada"),
    ENTRADA2_ABERTA(16, "Entrada 2 desativada"),
    ENTRADA3_ATIVADA(17, "Entrada 3 ativada"),
    ENTRADA3_ABERTA(18, "Entrada 3 desativada"),
    ENTRADA4_ATIVADA(19, "Entrada 4 ativada"),
    ENTRADA4_ABERTA(20, "Entrada 4 desativada"),
    GSENSOR_MOVIMENTO(21, "G-Sensor: Movimento"),
    GSENSOR_PARADO(22, "G-Sensor: Movimento"),
    ANTIFURTO_ALARMADO(23, "Antifurto alarmado"),
    FALHA_ACESSORIO(24, "Falha Acessorio"),
    FALHA_ENERGIA_EXTERNA(25, "Falha de Energia Externa"),
    ENERGIA_EXTERNA_OK(26, "Energia externa OK"),
    FALHA_ANTENA_GPS(27, "Falha Antena GPS"),
    ANTENA_GPS_OK(28, "Antena GPS OK"),
    PACOTE_RECEBIDO_ACESSORIO_WIRELESS(29, "Pacote Recebido por um acessorio Wireless"),
    ENTROU_MODO_SLEEP(30, "Entrou em Modo Sleep"),
    SAIDA1_ATIVADA(31, "SAIDA1_ATIVADA"),
    SAIDA1_DESATIVADA(32, "SAIDA1_DESATIVADA"),
    SAIDA2_ATIVADA(33, "SAIDA1_ATIVADA"),
    SAIDA2_DESATIVADA(34, "SAIDA1_DESATIVADA"),
    SAIDA3_ATIVADA(35, "SAIDA3_ATIVADA"),
    SAIDA3_DESATIVADA(36, "SAIDA3_DESATIVADA"),
    VELOCIDADE_MAXIMA_EXEDIDA(37, "Velocidade m\u00E1xima excedida"),
    VELOCIDADE_NORMALIZADA(38, "Velocidade normalizada"),
    ENTRADA_PONTO_REFERENCIA(39, " Entrada em ponto de refer\u00EAncia"),
    SAIDA_PONTO_REFERENCIA(40, "Sa\u00EDda de ponto de refer\u00EAncia"),
    FALHA_BATERIA_BACKUP(41, "Falha na bateria de Backup"),
    BATERIA_BACKUP_OK(42, "Bateria e Backup ok"),
    POSICAO_REENVIADA_POR_FALHA_ENVIO_PRIMEIRA_TENTATIVA(43, "Posi\u00E7\u00E3o reenviada por falha de envio na primeira tentativa"),
    POSICAO_REQUISITADA_SMS(44, "Posi\u00E7\u00E3o requisitada por SMS"),
    VIOLACAO_MODULO(45, "VIOLACAO_MODULO"),
    LIMITE_SENSOR_FRENTE_OU_TRAS_ATINGINDO(46, "Limite do sensor frente - tr\u00E1s atingido"),
    LIMITE_SENSOR_LATERAL_ATINGIDO(47, "Limite do sensor lateral atingido"),
    LIMITE_SENSOR_VERTICAL_ATINGIDO(48, "Limite do sensor vertical atingido"),
    ALTERACAO_DIRECAO_RECEBIDA_GPS(49, "Altera\u00E7\u00E3o na dire\u00E7\u00E3o recebida pelo GPS"),
    TRANSMISSAO_REALIZADA_MESMO_MOMENTO_ENVIO_POSICAO_SMS(50, "Transmiss\u00E3o realizada no mesmo momento do envio de uma posi\u00E7\u00E3o por SMS"),
    MXT_DESLIGADO(51, "MXT desligado"),
    ANTIFURTO_PASSA_ESTADO_NORMAL(52, "Antifurto passa para o estado normal"),
    DETECCAO_JAMMING(53, "Detec\u00E7\u00E3o de Jamming"),
    MODULO_DETECTA_QUE_NAO_ESTA_MAIS_SITUACAO_JAMMING(54, "M\u00F3dulo detecta que n\u00E3o est\u00E1 mais em situa\u00E7\u00E3o de Jamming"),
    ALTA_ROTACAO_RPM_MOVIMENTO(55, "Alta rota\u00E7\u00E3o (RPM) em movimento"),
    ALTA_ROTACAO_PONTO_NEUTRO(56, "Alta rota\u00E7\u00E3o em ponto neutro"),
    DETECCAO_VELOCIDADE_PONTO_NEUTRO(57, "Detec\u00E7\u00E3o de velocidade em ponto neutro(banguela)"),
    FALHA_GPS_SINCRONIZACAO_SATELITES(58, "Falha de GPS (sincroniza\u00E7\u00E3o com sat\u00E9lites)"),
    TRASMISSAO_POR_DISTANCIA(59, "Transmiss\u00E3o por dist\u00E2ncia"),
    FALHA_ALIMENTACAO_GPS(60, "Falha de alimenta\u00E7\u00E3o e GPS"),
    AGPS_REQUERIDO(61, "AGPS requerido"),
    STATUS_TAG_MUDOU(62, "Status da tag mudou"),
    STATUS_BATERIA_TAG_MUDOU(63, "Status da bateria da TAG mudou"),
    LINK_BROKEN(64, "Link Broken"),
    ENTRADA_EXPAND_MUDOU(65, "Entrada Expand mudou"),
    TAG_ACESSORIES_STATUS_CHANGED(66, "TAG accessories status changed from 0 to 1");

    private Integer id;
    private String descricao;

    public Integer getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public static TransmissionReasonEnum getById(Integer id) {
        for (TransmissionReasonEnum e : values()) {
            if (e.id.equals(id)) return e;
        }
        return null;
    }

    private TransmissionReasonEnum(Integer Id, String descricao) {
        this.id = Id;
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }

}
