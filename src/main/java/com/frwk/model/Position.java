package com.frwk.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Classe que representa posição de rastreamento para entrada no sistema Argos.
 * A implementaçao concreta depende do tipo de dispositivo utilizado (MaxTrack, Gosafe, etc)
 */
@Getter@Setter
@Component
public class Position implements Serializable {

    private static final long serialVersionUID = 1L;

    private String serial;
    private String driverID;
    private TransmissionReasonEnum transmissionReasonEnum;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private Integer direcao;
    private Integer altitude;
    private Integer protocolo;
    private BigDecimal velocidade;
    private Long hodometro;
    private Long horimetro;
    private Date dataHora;
    private Long memoryIndex;
    private boolean ignicao;
    private boolean panico;
    private boolean falhaBateria;
    private boolean antifurto;
    private boolean falhaGPS;
    private Integer rpm;
    private Double powerSupply;

    private boolean input1;
    private boolean input2;
    private boolean input3;
    private boolean input4;
    private String rs232;

    private boolean output1;
    private boolean output2;
    private boolean output3;
    private boolean output4;

    private String iccid;
    private Integer temperature;
    private boolean sinalGprs;

    private List<PositionAdditional> additional;


    @Override
    public String toString() {
        return "Position{" +
                "driverID='" + driverID + '\'' +
                ", transmissionReasonEnum=" + transmissionReasonEnum +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", direcao=" + direcao +
                ", altitude=" + altitude +
                ", protocolo=" + protocolo +
                ", velocidade=" + velocidade +
                ", hodometro=" + hodometro +
                ", horimetro=" + horimetro +
                ", dataHora=" + dataHora +
                ", memoryIndex=" + memoryIndex +
                ", ignicao=" + ignicao +
                ", panico=" + panico +
                ", falhaBateria=" + falhaBateria +
                ", antifurto=" + antifurto +
                ", falhaGPS=" + falhaGPS +
                ", rpm=" + rpm +
                ", powerSupply=" + powerSupply +
                ", input1=" + input1 +
                ", input2=" + input2 +
                ", input3=" + input3 +
                ", input4=" + input4 +
                ", rs232='" + rs232 + '\'' +
                ", output1=" + output1 +
                ", output2=" + output2 +
                ", output3=" + output3 +
                ", output4=" + output4 +
                ", iccid='" + iccid + '\'' +
                ", temperature=" + temperature +
                ", sinalGprs=" + sinalGprs +
                ", additional=" + additional +
                '}';
    }
}
