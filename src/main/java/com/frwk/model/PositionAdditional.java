package com.frwk.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter@Setter
public class PositionAdditional implements Serializable {


    private static final long serialVersionUID = 1L;
    private String serial;
    private Integer batteryLevel;
    private Integer buttonStatus;
    private Integer internalTemperature;



}
